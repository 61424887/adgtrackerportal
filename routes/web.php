<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CampaignController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::middleware(['auth'])->group(function () {
    Route::resource('campaigns', App\Http\Controllers\CampaignController::class);
});


Route::get('login', 'App\Http\Controllers\LoginController@create')->name('login');
Route::post('login', 'App\Http\Controllers\LoginController@authenticate');
Route::get('logout', 'App\Http\Controllers\LoginController@logout');