<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
    	$credentials = $request->only('email', 'password');

    	if (Auth::attempt($credentials)) {
    		$request->session()->regenerate();
    		return response()->json(['message' => 'login success', 'status' => 200]);
    	}

    	return response()->json(['message' => 'The provided credentials do not match our records.', 'status' => 401]);
    }

    // ------------------------------------------------------------------------

    public function create()
    {
    	return view('auth.login');
    }

    // ------------------------------------------------------------------------

    /**
	 * Log the user out of the application.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
    public function logout(Request $request)
    {
    	Auth::logout();

    	$request->session()->invalidate();

    	$request->session()->regenerateToken();

    	return redirect('/login');
    }
}
