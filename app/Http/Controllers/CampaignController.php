<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Eloquent\CampaignRepositoryEloquent as CampaignRepository;

class CampaignController extends Controller
{
    protected $campaign;

    public function __construct(CampaignRepository $campaign)
    {
        $this->campaign = $campaign;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->input('table_search');

        if(isset($search))
        {
            $results = $this->campaign->where('campaign_id', 'like', '%'.$search.'%')->paginate()->setPath('');
            $pagination = $results->appends(['table_search' => $search]);
        }
        else
        {
            $results = $this->campaign->paginate();
        }
        
        return view('campaigns.list', ['campaigns' => $results]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->campaign->getCampaignDetails($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->campaign->updateCampainDetails($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //-------------------------------------------------------------------------

    public function getCampaigns()
    {
        return $this->campaign->fetchCampaigns();
    }
}
