<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\CampaignRepository;
use App\Entities\Campaign;
use App\Validators\CampaignValidator;

/**
 * Class CampaignRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class CampaignRepositoryEloquent extends BaseRepository implements CampaignRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
      return Campaign::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
      $this->pushCriteria(app(RequestCriteria::class));
    }

    //-------------------------------------------------------------------------

    public function fetchCampaigns()
    {

      $url = 'http://api.adgtracker.com/pubapi.php';
      $fields_string = 'apikey=427eba7f08b03171599128629d6a6c17aa074816a8f9c0fb2104ca0e25f4a5f2&apifunc=getcampaigns';


      $ch = curl_init();

      curl_setopt($ch,CURLOPT_URL, $url);
      curl_setopt($ch,CURLOPT_POST, 1);
      curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $data = curl_exec($ch);

      if($data == "PLEASE WAIT A FEW MINUTES BEFORE NEXT API CALL")
      {
        return "PLEASE WAIT A FEW MINUTES BEFORE NEXT API CALL";
      }

      if (curl_errno($ch))
      {
        return curl_error($ch); 
      } 

      curl_close($ch);
      
      $xml = simplexml_load_string($data, "SimpleXMLElement", LIBXML_NOCDATA);

      $json = json_encode($xml);
      $arrCampaign = json_decode($json,TRUE);


      foreach ($arrCampaign['data'] as $key => $value) {
        $campaignId = trim($value['campaignid']);

        $campaignRes = $this->findByField('campaign_id', $campaignId);


        if(count($campaignRes) == 0)
        {
          $campaign = ['campaign_id' => $campaignId,
          'name' => trim($value['name']),
          'description' => (is_string($value['description']))?trim($value['description']):NULL,
          'payout' => (is_string($value['payout']))?trim($value['payout']):NULL,
          'unit' => (is_string($value['unit']))?trim($value['unit']):NULL,
          'category' => (is_string($value['category']))?trim($value['category']):NULL,
          'geotargeting' => (is_string($value['geotargeting']))?trim($value['geotargeting']):NULL];
          $this->model->create($campaign);

        } 
      }
      return 'success';
    }

    // ------------------------------------------------------------------------

    public function updateCampainDetails($data, $id)
    {
      $campaign = $this->where('campaign_id', $id)->first();
      $this->update(['redirect_link' => $data['inputRedirectLink']], $campaign->id);
      return response()->json(['message' => 'campaign details updated successfully', 'status' => 200]);
    }

    // ------------------------------------------------------------------------

    public function getCampaignDetails($id)
    {
      $campaign = $this->where('campaign_id', $id)->first();
      return response()->json(['redirectLink' => $campaign->redirect_link, 'status' => 200]);
    }
    
  }
