<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CampaignRepository.
 *
 * @package namespace App\Contracts\Repositories;
 */
interface CampaignRepository extends RepositoryInterface
{
    //
}
