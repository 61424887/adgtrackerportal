const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .postCss('resources/css/app.css', 'public/css', [
//         //
//     ]);

mix.styles(['resources/adminlte/plugins/fontawesome-free/css/all.min.css',
       'resources/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css',
       'resources/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css',
       'resources/adminlte/plugins/jqvmap/jqvmap.min.css',
       'resources/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
       'resources/adminlte/plugins/daterangepicker/daterangepicker.css',
       'resources/adminlte/plugins/summernote/summernote-bs4.min.css',
       'resources/adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css'],'public/css/vendor.css');

mix.styles(['resources/adminlte/dist/css/adminlte.min.css'], 'public/css/app.css');

mix.scripts(['resources/adminlte/plugins/jquery/jquery.min.js',
        'resources/adminlte/plugins/jquery-ui/jquery-ui.min.js',
        'resources/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js',
        'resources/adminlte/plugins/chart.js/Chart.min.js',
        'resources/adminlte/plugins/sparklines/sparkline.js',
        'resources/adminlte/plugins/jqvmap/jquery.vmap.min.js',
        'resources/adminlte/plugins/jqvmap/maps/jquery.vmap.usa.js',
        'resources/adminlte/plugins/jquery-knob/jquery.knob.min.js',
        'resources/adminlte/plugins/moment/moment.min.js',
        'resources/adminlte/plugins/daterangepicker/daterangepicker.js',
        'resources/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js',
        'resources/adminlte/plugins/summernote/summernote-bs4.min.js',
        'resources/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
        'resources/adminlte/plugins/sweetalert2/sweetalert2.min.js'], 'public/js/vendor.js');

mix.scripts(['resources/adminlte/dist/js/adminlte.js',
             'resources/campaign/editcampaign.js',
             'resources/auth/login.js'], 'public/js/app.js');