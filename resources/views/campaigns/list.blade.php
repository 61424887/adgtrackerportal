@extends('layouts.app')


@section('content')
<!-- /.row -->
<!-- Main row -->
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Campaigns List</h3>

        <div class="card-tools">
          <form method="GET" action="{{url('campaigns')}}" type="role">
            <div class="input-group input-group-sm">
              <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
          <thead>
            <tr>
              <th>CampaignID</th>
              <th>Name</th>
              <!-- <th>Description</th> -->
              <th>Payout</th>
              <th>Unit</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($campaigns as $campaign)
            <tr><td class="campaignId">{{$campaign->campaign_id}}</td>
              <td>{{$campaign->name}}</td>
              <!-- <td>{{$campaign->description}}</td> -->
              <td>{{$campaign->payout}}</td>
              <td>{{$campaign->unit}}</td>
              <td><button type="button" class="btn btn-default btn-sm getCampaignId" data-toggle="modal" data-target="#modal-lg">Edit</button></td></tr>
              @endforeach
            </tbody>
          </table>
          <div class="card-footer clearfix">
            <ul class="pagination pagination-sm m-0 float-right">
              {{ $campaigns->links() }}
            </ul>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>

  <div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Edit Campaign Details</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="campaignForm" name="campaignForm">
          <div class="modal-body">
            <div class="card-body">
              <div class="form-group">
                <label for="inputRedirectLink">Redirect Link</label>
                <input type="text" class="form-control" id="inputRedirectLink" name="inputRedirectLink" placeholder="Enter redirect link" autocomplete="off" value="">
                <input type="hidden" name="campaignId" value="" id="campaignId">
              </div>

            </div>
            <!-- /.card-body -->
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" id="submit">Save changes</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <!-- /.row -->
  <!-- (main row) -->
  @endsection