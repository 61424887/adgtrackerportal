$(document).ready(function(){
	var Toast = Swal.mixin({
		toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000
	});


	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$("button.getCampaignId").on("click", function(e){
		let row = $(this).closest('tr');
		let campaignId = row.find('.campaignId').text();
		$("#campaignId").val(campaignId);

		$.ajax({
			type: "GET",
			url: "http://127.0.0.1:8000/campaigns/"+campaignId,
			success: function(data)
			{
				$('#inputRedirectLink').val(data.redirectLink);
			},
			error: function(e)
			{
				//
			}
		});

		$("form").on("submit", function(event){
			event.preventDefault();
			let formValues= $(this).serialize();
			let id = $("#campaignId").val();

			$.ajax({
				type: "PUT",
				url: "http://127.0.0.1:8000/campaigns/"+id,
				data: formValues,
				success: function(data)
				{
					if(data.status == 200)
					{
						$('#modal-lg').modal('hide');
						$('#modal-lg').on('hidden.bs.modal', function () {
							$(this).find('form').trigger('reset');
						})

						Toast.fire({
							icon: 'success',
							title: data.message
						})
					}
				},
				error: function(e)
				{
					console.log(e);
				}
			});
		});
	});
});