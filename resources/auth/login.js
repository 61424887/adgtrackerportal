$(document).ready(function(){
	var Toast = Swal.mixin({
		toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000
	});


	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$("form#loginForm").on("submit", function(event){
		let formValues = $(this).serialize();
		event.preventDefault();

		$.ajax({
			type: "POST",
			url: "http://127.0.0.1:8000/login",
			data: formValues,
			success: function(data)
			{
				if(data.status == 200)
				{
					window.location.replace("http://127.0.0.1:8000/campaigns");
				}
				if(data.status == 401)
				{
					Toast.fire({
						icon: 'error',
						title: data.message
					});
				}
				
			},
			error: function(e)
			{
				console.log(e);
			}
		});
	});
});